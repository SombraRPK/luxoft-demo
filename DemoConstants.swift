//
//  Constants.swift
//  LuxorDemo
//
//  Created by Reivaj Gómez Pérez on 24/02/21.
//

import Foundation

struct DemoConstants {
    static let jsonFileName = "items"
    struct cellDetails {
        static let cellIdentifier = "ReusableCell"
        static let cellName = "AppointmentCell"
    }
    
    static let daysOfWeek = [
        1: "Monday",
        2: "Tuesday",
        3: "Wednesday",
        4: "Thursday",
        5: "Friday",
        6: "Saturday",
        7: "Sunday"
    ]
}
