//
//  demoManager.swift
//  LuxorDemo
//
//  Created by Reivaj Gómez Pérez on 24/02/21.
//

import Foundation

class DemoManager {
    func loadJSONData(filename: String) -> Data? {
        guard let url = Bundle.main.url(forResource: filename, withExtension: "json") else {
            return nil
        }
        return try? Data(contentsOf: url)
    }
    
    func decodeJSONData (data: Data) -> [Appointment]?{
        var appointments: [Appointment]?
        let jsonDecoder =  JSONDecoder()
        do {
            appointments = try jsonDecoder.decode([Appointment].self, from: data)
            //print (decodedJSON)
        } catch {
            print (error.localizedDescription)
        }
        return appointments
    }
    
    func getDayName(dayNumber: Int) -> String {
        return DemoConstants.daysOfWeek[dayNumber]!
    }
}
