//
//  jsonData.swift
//  LuxorDemo
//
//  Created by Reivaj Gómez Pérez on 24/02/21.
//

import Foundation

struct Appointment: Decodable {
    var title: String
    var details: AppointmentDetails
}
