//
//  appointmentDetails.swift
//  LuxorDemo
//
//  Created by Reivaj Gómez Pérez on 24/02/21.
//

import Foundation

struct AppointmentDetails: Decodable {
    var description: String
    var day: Int
}
