//
//  AppointmentCellTableViewCell.swift
//  LuxorDemo
//
//  Created by Reivaj Gómez Pérez on 24/02/21.
//

import UIKit

class AppointmentCell: UITableViewCell {
    
    var demoManager = DemoManager()

    @IBOutlet weak var appointmentTitle: UILabel!
    @IBOutlet weak var appointmentDescription: UILabel!
    @IBOutlet weak var appointmentDay: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
