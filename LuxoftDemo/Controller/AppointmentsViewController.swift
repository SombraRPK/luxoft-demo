//
//  ViewController.swift
//  LuxorDemo
//
//  Created by Reivaj Gómez Pérez on 24/02/21.
//

import UIKit

class AppointmentsViewController: UIViewController {
    
    @IBOutlet weak var appointmentsTableView: UITableView!
    
    var demoManager = DemoManager()
    var receivedAppointments = [Appointment]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        appointmentsTableView.dataSource = self
        appointmentsTableView.register(UINib(nibName: DemoConstants.cellDetails.cellName, bundle: nil), forCellReuseIdentifier: DemoConstants.cellDetails.cellIdentifier)
    }
}

// MARK: - Table View data source
extension AppointmentsViewController:  UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if let secureJsonData = demoManager.loadJSONData(filename: DemoConstants.jsonFileName) {
            if let appointments = demoManager.decodeJSONData(data: secureJsonData) {
                receivedAppointments = appointments
                return receivedAppointments.count
            }
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let appointmentCell = appointmentsTableView.dequeueReusableCell(withIdentifier: DemoConstants.cellDetails.cellIdentifier, for: indexPath) as! AppointmentCell
        setupCell(cell: appointmentCell, indexPath: indexPath)
        return appointmentCell
    }
    
    func setupCell(cell: AppointmentCell, indexPath: IndexPath) {
        cell.appointmentTitle.text = receivedAppointments[indexPath.row].title
        cell.appointmentDescription.text = receivedAppointments[indexPath.row].details.description
        cell.appointmentDay.text = demoManager.getDayName(dayNumber: (receivedAppointments[indexPath.row].details.day))
    }
}
